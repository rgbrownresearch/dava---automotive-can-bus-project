#include <stdio.h>
#include <time.h>
#include <stdlib.h>

typedef struct{
	int pos;
	int ids[2];
	int upper[2];
	int lower[2];
	int con;
} NODE;

int messageID=0;

int numNodes =2;

int updateControl =1;

int RandomID(int upper, int lower) { return rand() % (upper-lower) + lower;}

void sendMessage(int id) { messageID=id; printf("This is the message ID for the receiver 0x%d\n", id); getchar();}

int update(int p, NODE* n) 
{ 
	int x = RandomID(n->upper[p], n->lower[p]); 
	printf("Here is the new ID 0x%d\n", x); 
	getchar();
	return x; 
}

void receiveMessage(NODE* x, NODE* y) 
{
	if(messageID == x->ids[x->pos])
	{
		if(x->con == updateControl)
		{
		printf("I am ecu1 and I have received a message\nI will now broadcast update that ID 0x%d is invalid\n", x->ids[x->pos]);
		getchar();
		x->ids[x->pos] = y->ids[x->pos] = update(x->pos, x);
		x->con =1;
		}
		else
		{
			x->con++;
		}
	}
	else if(messageID == y->ids[y->pos])
	{
		if(y->con == updateControl)
		{
		printf("I am ecu2 and I have received a message\nI will now broadcast update that ID 0x%d is invalid\n", y->ids[y->pos]);
		getchar();
		x->ids[y->pos] = y->ids[y->pos] = update(y->pos, x);
		y->con=1;
		}
		else
		{
			y->con++;
		}
	}
}

int main(int argc, char* argv[])
{
	NODE ecu1, ecu2;
	ecu1.con = ecu2.con =1;
	ecu1.pos=0;
	ecu2.pos=1;
	ecu1.ids[0] = ecu2.ids[0] =1;
	ecu1.ids[1] = ecu2.ids[1] =500;
	ecu1.upper[0] = ecu2.upper[0] =499;
	ecu1.upper[1] = ecu2.upper[1] =999;
	ecu1.lower[0] = ecu2.lower[0] =1;
	ecu1.lower[1] = ecu2.lower[1] =500;
	srand(time(0));

	double x = (double)clock();
	RandomID(ecu1.upper[0], ecu1.lower[0]);
	double y = (double)clock();
	x= (double)(y-x)/CLOCKS_PER_SEC;
	printf("this is the average time for a random number to be generated and the update phase to be completed %.8lf seconds\n\n", x);
	
	while(1)
	{
		printf("ecu1 is sending a message to ecu2\n");
		getchar();
		sendMessage(ecu1.ids[1]);
		receiveMessage(&ecu1, &ecu2);
		printf("This is ecu2's new ID 0x%d from ecu2 0x%d from ecu1\n It has been updated in both ecus\n", ecu2.ids[1], ecu1.ids[1]);
		getchar();
		printf("ecu2 is sending a message to ecu1\n");
		getchar();
		sendMessage(ecu2.ids[0]);
		receiveMessage(&ecu1, &ecu2);
		printf("This is ecu1's new ID 0x%d from ecu1 0x%d from ecu2\n It has been updated in both ecus\n", ecu1.ids[0], ecu2.ids[0]);
	}
	return 0;
}
